#!/bin/bash

set -eo pipefail

source scripts/_common.sh

if "$CERTIFIED"; then
  OPERATOR_PROJECT="$REDHAT_OPERATOR_IMAGES_REPOSITORY"
  OPERATOR_BUNDLE_PROJECT="$GITLAB_RUNNER_OPERATOR_BUNDLE_CONNECT_NAMESPACE"
else
  OPERATOR_PROJECT="$GITLAB_RUNNER_OPERATOR_UPSTREAM_NAMESPACE"
  OPERATOR_BUNDLE_PROJECT="$GITLAB_RUNNER_OPERATOR_BUNDLE_UPSTREAM_NAMESPACE"
fi

echo "Building bundle for operator image $OPERATOR_PROJECT/gitlab-runner-operator$(image_digest "operator")"

BUNDLE_IMAGE="$OPERATOR_BUNDLE_PROJECT/gitlab-runner-operator-bundle:$OPERATOR_VERSION"
make IMG="$BUNDLE_IMAGE" bundle
source ./scripts/create_related_images_config.sh
source ./scripts/set_csv_versions.sh

# REVISION is already included in the $OPERATOR_VERSION that was passed down from the initial make target
make PROJECT="$OPERATOR_BUNDLE_PROJECT" VERSION="$OPERATOR_VERSION" REVISION="" \
 certification-bundle certification-bundle-build certification-bundle-tag
login_and_push bundle "$CERTIFIED"

# When pushing bundle images to the RedHat registry if a certification fails the image cannot be deleted.
# Official advice from RedHat is to push images with an incremented postfix in the format "bundle:v1.1.0-1"
for INDEX in {0..25}
do
  OVERRIDE_RELEASE_VERSION="$OPERATOR_VERSION"
  if [[ "$INDEX" -gt 0 ]]; then
      OVERRIDE_RELEASE_VERSION="$OVERRIDE_RELEASE_VERSION-$INDEX"
      echo "Trying patch build $OVERRIDE_RELEASE_VERSION since previous patch exists."
      docker tag "$BUNDLE_IMAGE" "$BUNDLE_IMAGE_OVERRIDE"
      login_and_push bundle "$CERTIFIED" "$OVERRIDE_RELEASE_VERSION"
  fi

  BUNDLE_IMAGE_OVERRIDE="$OPERATOR_BUNDLE_PROJECT/gitlab-runner-operator-bundle:$OVERRIDE_RELEASE_VERSION"
  if test "$(docker inspect --format='{{ index .RepoDigests 0 }}' "$BUNDLE_IMAGE_OVERRIDE" 2> /dev/null)"; then
    exit 0
  fi
done

